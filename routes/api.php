<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('login', array('as'=>'login','uses'=>'LoginController@login'));
Route::post('register', 'LoginController@register');

Route::middleware('auth:api')->post('details',array('uses'=>'LoginController@details'));



Route::post('event',array('uses'=>'CTLEventsController@addEvent'));
Route::put('event',array('uses'=>'CTLEventsController@updateEvent'));
Route::delete('event/{id}',array('uses'=>'CTLEventsController@deleteEvent'));
Route::get('event/{id}',array('uses'=>'CTLEventsController@getEvent'))->where('id', '[0-9]+');;
Route::get('event',array('uses'=>'CTLEventsController@getAllEvents'));
Route::get('event/filter/{search_value}',array('uses'=>'CTLEventsController@filter'));
//Route::get('event/filter/{search_value}/{order_column?}/{order_direction?}/{limit?}/{offset?}',array('uses'=>'CTLEventsController@filter'));


Route::post('verse',array('uses'=>'CTLVersesController@addVerse'));
Route::put('verse',array('uses'=>'CTLVersesController@updateVerse'));
Route::delete('verse/{id}',array('uses'=>'CTLVersesController@deleteVerse'));
Route::get('verse/{id}',array('uses'=>'CTLVersesController@getVerse'))->where('id', '[0-9]+');;
Route::get('verse/today',array('uses'=>'CTLVersesController@getTodaysVerse'));
Route::get('verse',array('uses'=>'CTLVersesController@getAllVerses'));
Route::get('verse/filter/{search_value}',array('uses'=>'CTLVersesController@filter'));
//Route::get('verse/filter/{search_value}/{order_column?}/{order_direction?}/{limit?}/{offset?}',array('uses'=>'CTLVersesController@filter'));



Route::get('event/image/{token}',array('uses'=>'ImagesController@getEventImage'));
Route::post('event/image/{token}',array('uses'=>'ImagesController@uploadEventImage'));


Route::get('verse/image/{token}',array('uses'=>'ImagesController@getVerseImage'));
Route::post('verse/image/{token}',array('uses'=>'ImagesController@uploadVerseImage'));
