<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVersesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',255)->nullable(false);
            $table->date('date')->nullable(false);
            $table->string('verse',255)->nullable(false);
            $table->longText('contents')->nullable(true);
            $table->longText('description')->nullable(true);
            $table->longText('encouragement')->nullable(true);
            $table->longText('image')->nullable(true);
            $table->string('token')->default("NO_TOKEN");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verses');
    }
}
