<?php

namespace CityLight\Listeners;

use CityLight\Events\CTLContentPosted;
use CityLight\MyClasses\PushNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Pusher\Pusher;

class CTLContentPostedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(CTLContentPosted $event)
    {
        $pushNotification = $event->pushNotification;

        $this->send($pushNotification);
    }

    private function send(PushNotification $pushNotification){

        $curl = curl_init();
        $data = $pushNotification->generateData();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://d9dfca93-bc88-4928-9ffa-220f99dabba7.pushnotifications.pusher.com/publish_api/v1/instances/d9dfca93-bc88-4928-9ffa-220f99dabba7/publishes",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                // Set here requred headers
                "Authorization: Bearer D32787E1E61E61D4B60F2D6C75655806BAD400C31C7FEA56B8E619B71FA93F2C",
                "content-type: application/json",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            Log::debug("MESSAGE SENT".$response);
        }
    }
}
