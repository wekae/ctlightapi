<?php
/**
 * Created by PhpStorm.
 * User: solunet
 * Date: 06/09/2018
 * Time: 11:47
 */

namespace CityLight\Listeners;


use CityLight\Events\FileUpload;
use CityLight\MyFacades\FilesFacade;

class FileUploadListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(FileUpload $event)
    {
        $file = $event->file;
        $file_name = $event->file_name;
        $path = $event->path;

        $this->upload($file, $file_name, $path);
    }

    private function upload($file, $file_name, $path){
        FilesFacade::upload($file, $file_name, $path);
    }

}