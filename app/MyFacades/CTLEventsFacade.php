<?php

namespace CityLight\MyFacades;


use Illuminate\Support\Facades\Facade;

class CTLEventsFacade extends Facade
{
    protected static function getFacadeAccessor(){
        return 'CTLEventsFacade';
    }

}