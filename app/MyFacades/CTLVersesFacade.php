<?php

namespace CityLight\MyFacades;


use Illuminate\Support\Facades\Facade;

class CTLVersesFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'CTLVersesFacade';
    }

}