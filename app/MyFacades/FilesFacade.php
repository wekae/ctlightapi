<?php


namespace CityLight\MyFacades;


use Illuminate\Support\Facades\Facade;

class FilesFacade extends Facade
{
    protected static function getFacadeAccessor(){
        return 'FilesFacade';
    }
}