<?php

namespace CityLight\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class FileFacadeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('FilesFacade', function(){
           return new \CityLight\MyClasses\FilesService;
        });
    }
}
