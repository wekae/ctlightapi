<?php

namespace CityLight\Providers;

use CityLight\MyClasses\CTLVerseService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class CTLVersesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('CTLVersesFacade', function(){
            return new \CityLight\MyClasses\CTLVerseService;
        });
    }
}
