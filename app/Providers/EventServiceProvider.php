<?php

namespace CityLight\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'CityLight\Events\Event' => [
            'CityLight\Listeners\EventListener',
        ],
        'CityLight\Events\CTLContentPosted' => [
            'CityLight\Listeners\CTLContentPostedListener',
        ],
        'CityLight\Events\FileUpload' => [
            'CityLight\Listeners\FileUploadListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
