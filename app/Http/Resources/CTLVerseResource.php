<?php

namespace CityLight\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CTLVerseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'title'=>$this->title,
            'date'=>$this->date,
            'verse'=>$this->verse,
            'contents'=>$this->contents,
            'description'=>$this->description,
            'encouragement'=>$this->encouragement,
            'image'=>$this->when($this->image, true, false),
            'token'=>$this->token,
        ];
    }
}
