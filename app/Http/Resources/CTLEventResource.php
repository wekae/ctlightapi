<?php

namespace CityLight\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CTLEventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'title'=>$this->title,
            'date'=>$this->date,
            'venue'=>$this->venue,
            'image'=>$this->when($this->image, true, false),
            'token'=>$this->token,
            'description'=>$this->description,
            'additional_info'=>$this->additional_info
        ];
    }
}
