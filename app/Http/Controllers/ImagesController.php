<?php

namespace CityLight\Http\Controllers;

use CityLight\CTLEventModel;
use CityLight\CTLVerseModel;
use CityLight\Http\Resources\CTLEventResource;
use CityLight\MyFacades\FilesFacade;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class ImagesController extends Controller
{


    public $createdStatus = 201;
    public $successStatus = 200;
    public $noContentStatus = 204;
    public $notImplementedStatus = 501;
    public $notAuthorizedStatus = 401;
    public $notFoundStatus = 404;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|null
     * Public url to access image resource
     */
    public function getEventImage(Request $request){
        $token = $request->token;

        $event = CTLEventModel::where('token','=',$token)->firstOrFail();
        if(!$event){
            return null;
        }

        if($event->image){
            $img = Image::make($event->image)->resize(300, 200);

            return $img->response('png') ;
        }

        $response['status']="Image not found";
        return response()->json(['error'=>$response], $this-> notFoundStatus);

    }

    /**
     * @param Request $request
     * Upload Event Image
     */
    public function uploadEventImage(Request $request){
        $token = $request->token;
        $image = $request->file('image');

        if($token && $image){
            /**
             * Stored in public folder
             */
            $path = "assets/img/event/thumbnail/";

            /**
             * Get Event
             */
            $event = CTLEventModel::where('token', $token)->first();

            /**
             * Perform upload if image exists
             */
            if($event){
                $this->uploadImage($image, $event, $path);
            }else{
                $response['status']="Event not found";
                return response()->json(['error'=>$response], $this-> notFoundStatus);
            }

            $response['status']="Event image uploaded";
            return response()->json(['success'=>$response], $this-> successStatus);
        }

        $response['status']="Event image not uploaded";
        return response()->json(['error'=>$response], $this-> notImplementedStatus);
    }



    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|null
     * Public url to access image resource
     */
    public function getVerseImage(Request $request){
        $token = $request->token;

        $verse = CTLVerseModel::where('token','=',$token)->firstOrFail();
        if(!$verse){
            return null;
        }

        if($verse->image){
            $img = Image::make($verse->image)->resize(300, 200);

            return $img->response('png') ;
        }

        $response['status']="Image not found";
        return response()->json(['error'=>$response], $this-> notFoundStatus);

    }

    /**
     * @param Request $request
     * Upload Verse Image
     */
    public function uploadVerseImage(Request $request){
        $token = $request->token;
        $image = $request->file('image');

        if($token && $image){
            /**
             * Stored in public folder
             */
            $path = "assets/img/verse/thumbnail/";

            /**
             * Get Event
             */
            $verse = CTLVerseModel::where('token', $token)->first();

            /**
             * Perform upload if image exists
             */
            if($verse){
                $this->uploadImage($image, $verse, $path);
            }else{
                $response['status']="Verse not found";
                return response()->json(['error'=>$response], $this-> notFoundStatus);
            }

            $response['status']="Verse image uploaded";
            return response()->json(['success'=>$response], $this-> successStatus);
        }

        $response['status']="Verse image not uploaded";
        return response()->json(['error'=>$response], $this-> notImplementedStatus);
    }




    /**
     * @param $image
     * @param $model ELoquent Model
     * @param $path
     */
    private function uploadImage($image, Model $model, $path){
        $token = $model->token;

        /**
         * Upload Image
         */
        if($image && $model && $path){

            $file_name = $this->upload($image, $token, $path);

            /**
             * Update verse information
             */
            $model->image = $path.$file_name;

            $model->saveOrFail();
        }
    }

    /**
     * @param $file
     * @param $file_name
     * @param $path
     *
     * Upload Logic
     */
    private function upload($file, $file_name, $path){
        /**
         * Get file extension
         */
        $extension = FilesFacade::fileExtension($file);
        $file_name = $file_name.".".$extension;



        FilesFacade::upload($file, $file_name, $path);

        return $file_name;
    }
}
