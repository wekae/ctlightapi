<?php

namespace CityLight\Http\Controllers;

use CityLight\CTLEventModel;
use CityLight\Http\Resources\CTLEventsCollection;
use CityLight\Http\Resources\CTLEventResource;
use CityLight\MyFacades\CTLEventsFacade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CTLEventsController extends Controller
{

    public $createdStatus = 201;
    public $successStatus = 200;
    public $noContentStatus = 204;
    public $notImplementedStatus = 501;
    public $notFoundStatus = 404;


    /**
     * @POST
     * @param title
     * @param date
     * @param venue
     * @param description
     * @param additional_info
     *
     * @return
     *
     * Adds a new Event
     */
    public function addEvent(Request $request){
        $validator = Validator::make($request->all(),[
            'title'=>'required',
            'date'=>'date',
            'venue'=>'required',
            'description'=>'nullable',
            'additional_info'=>'nullable',
            'image'=>'nullable',
        ]);


        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $saved = CTLEventsFacade::new($request);

        if($saved){
            $response['status']="Event Created";

            return response()->json(['success'=>$response], $this-> createdStatus);
        }else{
            $response['status']="Event Not Created";
            return response()->json(['error'=>$response], $this-> notImplementedStatus);
        }



    }

    /**
     * @PUT
     * @param title
     * @param date
     * @param venue
     * @param description
     * @param additional_info
     *
     * @return
     *
     * Updates an Event
     */
    public function updateEvent(Request $request){
        $validator = Validator::make($request->all(),[
            'id'=>'nullable',
            'title'=>'nullable',
            'date'=>'date|nullable',
            'venue'=>'nullable',
            'description'=>'nullable',
            'additional_info'=>'nullable',
            'image'=>'nullable',
        ]);


        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $saved = CTLEventsFacade::update($request);

        if($saved){
            $response['status']="Event Updated";

            return response()->json(['success'=>$response], $this-> successStatus);
        }else{
            $response['status']="Update Failed";
            return response()->json(['error'=>$response], $this-> notImplementedStatus);
        }

    }


    /**
     * @DELETE
     * @param id
     *
     * @return
     *
     * Deletes An Event
     */
    public function deleteEvent(Request $request){

        $deleted = CTLEventsFacade::delete($request);

        if($deleted){
            $response['status']="Event Deleted";

            return response()->json(['success'=>$response], $this-> createdStatus);
        }else{
            $response['status']="Delete Failed";
            return response()->json(['error'=>$response], $this-> notImplementedStatus);
        }

    }

    /**
     * @GET
     * @param id
     *
     * @return CTLEventResource
     *
     * Returns event specified by the ID
     */
    public function getEvent(Request $request){

        $event = CTLEventsFacade::get($request);

        if($event){

            return new CTLEventResource($event);

        }else{
            $response['status']="Event not found";
            return response()->json(['error'=>$response], $this-> notFoundStatus);
        }

    }

    /**
     * @GET
     *
     * @return CTLEventsCollection
     *
     * Returns all events
     */
    public function getAllEvents(Request $request){

        $events = CTLEventsFacade::getAll($request);

        if($events->count()>0){

            return new CTLEventsCollection($events);

        }else{
            $response['status']="Events not found";
            return response()->json(['error'=>$response], $this-> notFoundStatus);
        }

    }


    /**
     * @GET
     * @param search_value
     * @param order_column
     * @param order_direction
     * @param limit
     * @param offset
     *
     * @return CTLEventsCollection
     *
     * Returns a filtered result set based on the search value
     */
    public function filter(Request $request){

//        $search_value = $request->search_value;
//        $order_column = $request->order_column;
//        $order_direction = $request->order_direction;
//        $limit = $request->limit;
//        $offset = $request->offset;

        $events = CTLEventsFacade::filter($request);

        if($events->count()>0){

            return new CTLEventsCollection($events);

        }else{
            $response['status']="Events not found Limit:";
            return response()->json(['error'=>$response], $this-> notFoundStatus);
        }

    }


}
