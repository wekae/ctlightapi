<?php

namespace CityLight\Http\Controllers;

use CityLight\Http\Resources\CTLVersesCollection;
use CityLight\Http\Resources\CTLVerseResource;
use CityLight\MyFacades\CTLVersesFacade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CTLVersesController extends Controller
{

    public $createdStatus = 201;
    public $successStatus = 200;
    public $noContentStatus = 204;
    public $notImplementedStatus = 501;
    public $notFoundStatus = 404;


    /**
     * @POST
     * @param title
     * @param date
     * @param verse
     * @param contents
     * @param description
     * @param encouragement
     *
     * @return
     *
     * Adds a new Verse
     */
    public function addVerse(Request $request){
        $validator = Validator::make($request->all(),[
            'title'=>'required',
            'date'=>'date',
            'verse'=>'required',
            'contents'=>'nullable',
            'description'=>'nullable',
            'encouragement'=>'nullable',
            'image'=>'nullable',
        ]);


        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $saved = CTLVersesFacade::new($request);

        if($saved){
            $response['status']="Verse Created";

            return response()->json(['success'=>$response], $this-> createdStatus);
        }else{
            $response['status']="Verse Not Created";
            return response()->json(['error'=>$response], $this-> notImplementedStatus);
        }



    }

    /**
     * @PUT
     * @param title
     * @param date
     * @param verse
     * @param contents
     * @param description
     * @param encouragement
     *
     * @return
     *
     * Updates a Verse
     */
    public function updateVerse(Request $request){
        $validator = Validator::make($request->all(),[
            'title'=>'required',
            'date'=>'date|nullable',
            'verse'=>'nullable',
            'contents'=>'nullable',
            'description'=>'nullable',
            'encouragement'=>'nullable',
            'image'=>'nullable',
        ]);


        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $saved = CTLVersesFacade::update($request);

        if($saved){
            $response['status']="Verse Updated";

            return response()->json(['success'=>$response], $this-> successStatus);
        }else{
            $response['status']="Update Failed";
            return response()->json(['error'=>$response], $this-> notImplementedStatus);
        }

    }


    /**
     * @DELETE
     * @param id
     *
     * @return
     *
     * Deletes A Verse
     */
    public function deleteVerse(Request $request){

        $deleted = CTLVersesFacade::delete($request);

        if($deleted){
            $response['status']="Verse Deleted";

            return response()->json(['success'=>$response], $this-> createdStatus);
        }else{
            $response['status']="Delete Failed";
            return response()->json(['error'=>$response], $this-> notImplementedStatus);
        }

    }

    /**
     * @GET
     * @param id
     *
     * @return CTLVerseResource
     *
     * Returns verse specified by the ID
     */
    public function getVerse(Request $request){

        $verse = CTLVersesFacade::get($request);

        if($verse){

            return new CTLVerseResource($verse);

        }else{
            $response['status']="Verse not found";
            return response()->json(['error'=>$response], $this-> notFoundStatus);
        }

    }

    /**
     * @GET
     * @param id
     *
     * @return CTLVerseResource
     *
     * Returns verse specified by the ID
     */
    public function getTodaysVerse(Request $request){

        $verse = CTLVersesFacade::getTodaysVerse($request);

        if($verse){

            return new CTLVerseResource($verse);

        }else{
            $response['status']="Verse not found";
            return response()->json(['error'=>$response], $this-> notFoundStatus);
        }

    }

    /**
     * @GET
     *
     * @return CTLVersesCollection
     *
     * Returns all verses
     */
    public function getAllVerses(Request $request){

        $verses = CTLVersesFacade::getAll($request);

        if($verses->count()>0){

            return new CTLVersesCollection($verses);

        }else{
            $response['status']="Verses not found";
            return response()->json(['error'=>$response], $this-> notFoundStatus);
        }

    }


    /**
     * @GET
     * @param search_value
     * @param order_column
     * @param order_direction
     * @param limit
     * @param offset
     *
     * @return CTLVersesCollection
     *
     * Returns a filtered result set based on the search value
     */
    public function filter(Request $request){

//        $search_value = $request->search_value;
//        $order_column = $request->order_column;
//        $order_direction = $request->order_direction;
//        $limit = $request->limit;
//        $offset = $request->offset;

        $verses = CTLVersesFacade::filter($request);

        if($verses->count()>0){

            return new CTLVersesCollection($verses);

        }else{
            $response['status']="Events not found";
            return response()->json(['error'=>$response], $this-> notFoundStatus);
        }

    }


}
