<?php




namespace CityLight\MyClasses;


use Illuminate\Support\Facades\Storage;

class FilesService
{
    public function fileName($file){
        $file_name = $file->getClientOriginalName();
//        $file_name = $file->getClientOriginalName();
        return $file_name;
    }

    public function fileExtension($file){
        $file_extension = $file->getClientOriginalExtension();
        return $file_extension;
    }

    public function realPath($file){
        $real_path = $file->getRealPath();
        return $real_path;
    }

    public function size($file){
        $size = $file->getSize();
        return $size;
    }

    public function mimeType($file){
        $mime_type = $file->getMimeType();
        return $mime_type;
    }

    public function upload($file, $file_name, $path){
        $file->move($path, $file_name);
    }

    public function delete($image){
        Storage::delete($image);
    }

}