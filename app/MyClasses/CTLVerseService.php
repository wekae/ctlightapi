<?php

namespace CityLight\MyClasses;


use CityLight\CTLVerseModel;
use CityLight\Events\CTLContentPosted;
use CityLight\MyFacades\FilesFacade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class CTLVerseService
{
    /**
     * @param Request $request
     * @return bool
     *
     * Handles Creation of a new verse
     */
    public function new(Request $request){
        $title = $request->title;
        $date = $request->date;
        $bverse = $request->verse;
        $contents = $request->contents;
        $description = $request->description;
        $encouragement = $request->encouragement;
        $image = $request->image;


        Log::debug('VERSE: '
            ."\n TITLE: ".$title
            ."\n Date: ".$date
            ."\n Verse: ".$bverse
            ."\n Contents: ".$contents
            ."\n Description ".$description
            ."\n Encouragement: ".$encouragement
        );

        $token=null;

        $verse= new CTLVerseModel();
        $verse->title = $title;
        $verse->date = $date;
        $verse->verse = $bverse;
        $verse->contents = $contents;
        $verse->description = $description;
        $verse->encouragement = $encouragement;

        $saved = $verse->saveOrFail();

        /**
         * Save token
         */
        if($saved){

            /**
             * Generate token
             */
            $token = $this->generate_token($verse->id.time().$title);

            /**
             * Update verse record
             */
            $verse->token=$token;
            $verse->saveOrFail();
        }

        /**
         * Upload Image
         */
        if($saved && $token && $image){

            /**
             * Stored in public folder
             */
            $path = "assets/img/verse/thumbnail/";

            $file_name = $this->upload($image, $token, $path);

            /**
             * Update verse information
             */
            $verse->image = $path.$file_name;

            $verse->saveOrFail();
        }



        /**
         * Fire event to send push notifications
         */
        if($saved){
            $pushNotification = new PushNotification();

            $pushNotification->setInterests("ctlverse");

            $notification['title'] = "New Verse";
            $notification['body'] = $title;
            $notification['icon'] = "ic_ctlight_logo";
            $data['id'] = $verse->id."";
            $data['redirect'] = "ctlverse";

            $pushNotification->setNotification($notification);
            $pushNotification->setData($data);

            event(new CTLContentPosted($pushNotification));
        }


        return $saved;
    }

    /**
     * @param Request $request
     * @return bool
     *
     * Handles updating a verse
     */
    public function update(Request $request){
        $id = $request->id;
        $title = $request->title;
        $date = $request->date;
        $verse = $request->verse;
        $contents = $request->contents;
        $description = $request->description;
        $encouragement = $request->encouragement;
        $image = $request->image;

        $verseModel = CTLVerseModel::find($id);
        if(!$verseModel){
            return false;
        }

        if($title){
            $verseModel->title = $title;
        }
        if($date){
            $verseModel->date = $date;
        }
        if($verse){
            $verseModel->verse = $verse;
        }
        if($contents){
            $verseModel->contents = $contents;
        }
        if($description){
            $verseModel->description = $description;
        }
        if($encouragement){
            $verseModel->encouragement = $encouragement;
        }


        $saved = $verseModel->saveOrFail();


        if($saved && $image){
            /**
             * Upload Image
             */

            /**
             * Stored in public folder
             */
            $path = "assets/img/verse/thumbnail/";

            /**
             * Get stored token
             */
            $token = $verse->token;

            $file_name = $this->upload($image, $token, $path);
        }

        return $saved;
    }

    /**
     * @param Request $request
     * @return bool
     *
     * Handles deleting a verse
     */
    public function delete(Request $request){
        $id = $request->id;
        $image="";

        $verse = CTLVerseModel::find($id);

        /**
         * Get image path
         */
        if($verse){
            $image = $verse->image;
        }

        $deleted = false;

        if($verse){
            $deleted = $verse->delete();
        }

        if($deleted){
            FilesFacade::delete($image);
        }

        return $deleted;
    }


    /**
     * @param Request $request
     * @return CTLVerseModel
     *
     * Handles retrieving a verse
     */
    public function get(Request $request){
        $id = $request->id;

        $verse = CTLVerseModel::find($id);
        if(!$verse){
            return null;
        }

        return $verse;
    }

    /**
     * @param Request $request
     * @return CTLVerseModel
     *
     * Handles retrieving a verse
     */
    public function getTodaysVerse(Request $request){


        $verse = CTLVerseModel::orderBy('created_at', 'desc')->first();
        if(!$verse){
            return null;
        }

        return $verse;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     *
     * Returns all verses
     */
    public function getAll(Request $request){
        $order_column = $request->order_column;
        $order_direction = $request->order_direction;
        $limit = $request->limit;
        $offset = $request->offset;

        if($order_column==null){
            $order_column="id";
        }
        if($order_direction==null){
            $order_direction="desc";
        }
        if($limit==null){
            $limit=100000;
        }
        if($offset==null){
            $offset=0;
        }


        $allVerses = CTLVerseModel::orderBy($order_column, $order_direction)
            ->take($limit)
            ->skip($offset)
            ->get();

        return $allVerses;
    }

    /**
     * @param $search_value
     * @param null $order_column
     * @param null $order_direction
     * @param null $limit
     * @param null $offset
     * @return CTLVerseModel
     *
     * Handles filter logic for retrieving verses
     */
    public function filter(Request $request){


        $search_value = $request->search_value;
        $order_column = $request->order_column;
        $order_direction = $request->order_direction;
        $limit = $request->limit;
        $offset = $request->offset;


        if($order_column==null){
            $order_column="id";
        }
        if($order_direction==null){
            $order_direction="asc";
        }
        if($limit==null){
            $limit=10;
        }
        if($offset==null){
            $offset=0;
        }

        $columns_array = array (
            "title",
            "date",
            "verse",
            "contents",
            "description",
            "encouragement"
        );

        $data = CTLVerseModel::select();


        /**
         * Filter data based on the search query
         */
        if($search_value){
            /**
             * create a nested OR clause
             */
            $data = $data->where(function($query) use($columns_array, $search_value){
                /**
                 * append each table column to the query
                 */
                foreach ($columns_array as $column){
                    $query->orWhere($column,'like','%'.$search_value.'%');
                }

            });
        }


        /**
         * Set ordering
         */
        $data = $data->orderBy($order_column, $order_direction);

        /**
         * Set limit and offset for pagination
         */
        $data = $data
            ->skip($offset)
            ->take($limit);


        /**
         * Get the filtered records
         */
        $data = $data->get();

        return $data;

    }


    /**
     * @param $file
     * @param $file_name
     * @param $path
     *
     * Upload Logic
     */
    private function upload($file, $file_name, $path){
        /**
         * Get file extension
         */
        $extension = FilesFacade::fileExtension($file);
        $file_name = $file_name.".".$extension;



        FilesFacade::upload($file, $file_name, $path);

        return $file_name;
    }


    /**
     * @param $key
     * @return string
     *
     * Token generation logic
     */
    private function generate_token($key){
        $token = hash_hmac('sha256', Str::random(7), $key);

        return $token;
    }
}