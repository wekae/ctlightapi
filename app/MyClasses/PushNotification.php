<?php

namespace CityLight\MyClasses;


class PushNotification
{
    private $interests;
    private $notification;
    private $data;

    /**
     * @return mixed
     */
    public function getInterests()
    {
        return $this->interests;
    }

    /**
     * @return mixed
     */
    public function getNotification()
    {
        return $this->notification;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $interests
     */
    public function setInterests($interests)
    {
        $this->interests = $interests;
    }

    /**
     * @param mixed $notification
     */
    public function setNotification($notification)
    {
        $this->notification = $notification;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }





    public function generateData(){
        $notification = array(
            "interests"=>array($this->interests),
            "fcm"=>array(
                "notification"=>$this->notification,
                "data"=>$this->data
            )
        );

        return $notification;
    }

}