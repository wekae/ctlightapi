<?php

namespace CityLight\MyClasses;


use CityLight\CTLEventModel;
use CityLight\Events\CTLContentPosted;
use CityLight\MyFacades\FilesFacade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class CTLEventService
{
    /**
     * @param Request $request
     * @return bool
     *
     * Handles Creation of a new event
     */
    public function new(Request $request){
        $title = $request->title;
        $date = $request->date;
        $venue = $request->venue;
        $description = $request->description;
        $additional_info = $request->additional_info;
        $image = $request->image;


        if($image){
            Log::debug('IMAGE: '.$image->getClientOriginalExtension());
        }


        $token=null;

        $event = new CTLEventModel();
        $event->title = $title;
        $event->date = $date;
        $event->venue = $venue;
        $event->description = $description;
        $event->additional_info = $additional_info;

        $saved = $event->saveOrFail();

        /**
         * Save token
         */
        if($saved){

            /**
             * Generate token
             */
            $token = $this->generate_token($event->id.time().$title);

            /**
             * Update event record
             */
            $event->token = $token;
            $event->saveOrFail();
        }

        /**
         * Upload Image
         */
        if($saved && $token && $image){

            /**
             * Stored in public folder
             */
            $path = "assets/img/event/thumbnail/";

            $file_name = $this->upload($image, $token, $path);

            /**
             * Update verse information
             */
            $event->image = $path.$file_name;

            $event->saveOrFail();
        }




        /**
         * Fire event to send push notifications
         */
        if($saved){
            $pushNotification = new PushNotification();

            $pushNotification->setInterests("ctlevent");

            $notification['title'] = "New Event";
            $notification['body'] = $title;
            $notification['icon'] = "ic_ctlight_logo";
            $data['id'] = $event->id;
            $data['redirect'] = "event";

            $pushNotification->setNotification($notification);
            $pushNotification->setData($data);

            event(new CTLContentPosted($pushNotification));
        }

        return $saved;
    }

    /**
     * @param Request $request
     * @return bool
     *
     * Handles updating an event
     */
    public function update(Request $request){
        $id = $request->id;
        $title = $request->title;
        $date = $request->date;
        $venue = $request->venue;
        $description = $request->description;
        $additional_info = $request->additional_info;
        $image = $request->image;

        $event = CTLEventModel::find($id);
        if(!$event){
            return false;
        }

        if($title){
            $event->title = $title;
        }
        if($date){
            $event->date = $date;
        }
        if($venue){
            $event->venue = $venue;
        }
        if($description){
            $event->description = $description;
        }
        if($additional_info){
            $event->additional_info = $additional_info;
        }

        $saved = $event->saveOrFail();

        Log::debug("UPDATED: ".$saved);

//        if($saved && $image){
        if($image){
            /**
             * Upload Image
             */

            /**
             * Stored in public folder
             */
            $path = "assets/img/event/thumbnail/";

            /**
             * Get stored token
             */
            $token = $event->token;

            Log::debug("IMAGE: ".$image->getClientOriginalExtension());
            Log::debug("TOKEN: ".$token);

            $file_name = $this->upload($image, $token, $path);
        }

        return $saved;
    }

    /**
     * @param Request $request
     * @return bool
     *
     * Handles deleting an event
     */
    public function delete(Request $request){
        $id = $request->id;
        $image="";

        $event = CTLEventModel::find($id);

        /**
         * Get image path
         */
        if($event){
            $image = $event->image;
        }

        $deleted = false;

        if($event){
            $deleted = $event->delete();
        }

        if($deleted){
            FilesFacade::delete($image);
        }

        return $deleted;
    }


    /**
     * @param Request $request
     * @return CTLEventModel
     *
     * Handles retrieving an event
     */
    public function get(Request $request){
        $id = $request->id;

        $event = CTLEventModel::find($id);
        if(!$event){
            return null;
        }

        return $event;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     *
     * Returns all events
     */
    public function getAll(Request $request){


        $order_column = $request->order_column;
        $order_direction = $request->order_direction;
        $limit = $request->limit;
        $offset = $request->offset;

        if($order_column==null){
            $order_column="id";
        }
        if($order_direction==null){
            $order_direction="desc";
        }
        if($limit==null){
            $limit=100000;
        }
        if($offset==null){
            $offset=0;
        }


        $allEvents = CTLEventModel::orderBy($order_column, $order_direction)
            ->take($limit)
            ->skip($offset)
            ->get();

        return $allEvents;
    }

    /**
     * @param $search_value
     * @param null $order_column
     * @param null $order_direction
     * @param null $limit
     * @param null $offset
     * @return CTLEventModel
     *
     * Handles filter logic for retrieving events
     */
    public function filter(Request $request){


        $search_value = $request->search_value;
        $order_column = $request->order_column;
        $order_direction = $request->order_direction;
        $limit = $request->limit;
        $offset = $request->offset;

        if($order_column==null){
            $order_column="id";
        }
        if($order_direction==null){
            $order_direction="desc";
        }
        if($limit==null){
            $limit=10;
        }
        if($offset==null){
            $offset=0;
        }

        $columns_array = array (
            "title",
            "date",
            "venue",
            "description",
            "additional_info"
        );

        $data = CTLEventModel::select();


        /**
         * Filter data based on the search query
         */
        if($search_value){
            /**
             * create a nested OR clause
             */
            $data = $data->where(function($query) use($columns_array, $search_value){
                /**
                 * append each table column to the query
                 */
                foreach ($columns_array as $column){
                    $query->orWhere($column,'like','%'.$search_value.'%');
                }

            });
        }


        /**
         * Set ordering
         */
            $data = $data->orderBy($order_column, $order_direction);

        /**
         * Set limit and offset for pagination
         */
        $data = $data
            ->skip($offset)
            ->take($limit);


        /**
         * Get the filtered records
         */
        $data = $data->get();

        return $data;

    }

    /**
     * @param $file
     * @param $file_name
     * @param $path
     *
     * Upload Logic
     */
    private function upload($file, $file_name, $path){
        /**
         * Get file extension
         */
        $extension = FilesFacade::fileExtension($file);
        $file_name = $file_name.".".$extension;



        FilesFacade::upload($file, $file_name, $path);

        return $file_name;
    }


    /**
     * @param $key
     * @return string
     *
     * Token generation logic
     */
    private function generate_token($key){
        $token = hash_hmac('sha256', Str::random(7), $key);

        return $token;
    }

}