<?php

namespace CityLight\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class FileUpload
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $file;
    public $file_name;
    public $path;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($file, $file_name, $path)
    {
        $this->file = $file;
        $this->file_name = $file_name;
        $this->path = $path;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
