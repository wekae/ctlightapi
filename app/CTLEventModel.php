<?php

namespace CityLight;

use Illuminate\Database\Eloquent\Model;

class CTLEventModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'events';

    /**
     * Used to set items per page in pagination
     *
     * @var string
     */
    protected $perPage=12;
}
